heroku maintenance:on
git push heroku
heroku run rake db:migrate
heroku config:set HEROKU_URL=$(heroku info -s | grep web_url | cut -d= -f2 | sed 's/https\?:\/\///' | sed s/'\/'/''/g)
heroku maintenance:off