require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
  end

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get help" do
    get :help
    assert_response :success
    if I18n.locale == :en
      @help_title = "Help | #{@base_title}"
    else
      @help_title = "幫助 | #{@base_title}"
    end
    assert_select "title", "#{@help_title}"
  end

  test "should get about" do
    get :about
    assert_response :success
    if I18n.locale == :en
      assert_select "title", "About | #{@base_title}"
    else
      assert_select "title", "關於我們 | #{@base_title}"
    end
  end

  test "should get contact" do
    get :contact
    assert_response :success
    if I18n.locale == :en
      assert_select "title", "Contact | #{@base_title}"
    else
      assert_select "title", "聯絡我們 | #{@base_title}"
    end
  end
end
